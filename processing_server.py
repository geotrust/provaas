import boto.sqs
from boto.sqs.message import Message
from time import sleep

from help_json import *
from geoprovdm import *
from datetime import datetime
import argparse
from logging.handlers import TimedRotatingFileHandler
import logging
import json
import configparser

ENVIRON = os.environ.get('PROV_ENVIRON')
if (ENVIRON == None):
   ENVIRON = 'PROD'

# global variables will be initialized in setup_golbal_variables()
db = provaas_queue = logger = config = None
'''
run with arguments '--test 1' to obtain TEST mode
nohup python processing_server.py >>processing_1.log 2>&1 &
'''

def setup_golbal_variables():
    global db, provaas_queue, logger, config
    try:
        config_file_name = os.path.join(os.getcwd(),'Config.ini')
        config = configparser.ConfigParser()
        config.read(os.path.abspath(config_file_name))

        SERVER_IP = config['Default']['SERVER_IP']
        access_key = config['Default']['AWS_ACCESS_KEY']
        secret_key = config['Default']['AWS_SECRET_KEY']
        queue_region = config['Default']['AWS_QUEUE_REGION']
        queue_name = config['Default']['AWS_QUEUE_NAME']

        file_name = config['Default']['PROCESSING_LOG_FILENAME']

    except:
        print sys.exc_info()
        exit(1)

    # AWS SQS
    conn = boto.sqs.connect_to_region(queue_region, aws_access_key_id=access_key, aws_secret_access_key = secret_key)
    provaas_queue = conn.create_queue(queue_name)

    #in this case, neo4j server and flask server are on the same machine, same SERVER_IP
    db = GeoProvDM(ENVIRON, "http://%s:7474/db/data/"%SERVER_IP, False)

    LOG_FORMAT = '%(asctime)-15s [%(levelname)-7s] %(message)s'
    handler = TimedRotatingFileHandler(file_name,when="d",interval=1)
    formatter = logging.Formatter(LOG_FORMAT)
    handler.setFormatter(formatter)
    handler.setLevel(logging.INFO)
    logger = logging.getLogger("processing_server")
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    console.setFormatter(formatter)
    logger.addHandler(console)


def process_delete_by_uuid(namespace,uuid):
    obj = db.deleteNodeByUuid(namespace,uuid)
    if (obj == True):
        data = {"Deleted:": obj}
    else:
        data = {"Deleted:": obj, "Reason": "namespace or uuid does not exist"}
    logger.info(str(data))

def process_delete_by_rid(namespace,rid):
    obj = db.deleteNodeByRequestid(namespace,rid)
    obj_json = neo2json(obj)

    data = {"Deleted:": obj_json}
    logger.info(str(data))

def process_post(obj):
    entities = obj['entity']
    for k in entities.keys():
        entity = json2obj(entities[k])
        entity[u'_id'] = k
        node = db.addEntity(entity)
        #db.addProperty(node,entity)

    # make all agents
    #agents = obj['agent']
    #for k in agents.keys():
    #    agent = json2obj(agents[k])
    #    agent[u'_id'] = k
    #    db.addAgent(agent)

    acts = obj['activity']
    for k in acts.keys():
        act = json2obj(acts[k])
        act[u'_id'] = k
        db.addActivity(act)

    # =========================
    # === add all relations ===
    for rel in db.getRequiredIdsInRelation().keys():
        try:
            relations = obj[rel]
            for name in relations.keys():
                db.addRelation(rel, name, relations[name])
        except KeyError:
            pass
    data = {"Message:": "Post completed successfully"}
    logger.info(str(data))

def print_statistics(posts, deletes, unknowns):
    logger.info("post: %s, delete: %s, unknown: %s"%(str(posts), str(deletes), str(unknowns)))

def processing_loop(isTestMode):
    bRequiresNewLine = False

    max_processing_seconds = 60
    waiting_seconds = 30
    count_delete = 0
    count_post = 0
    count_unknown = 0

    debug_ips = [x.strip() for x in config['Default']['DEBUG_IPS'].split(',')]
    if isTestMode:
        logger.info("Processing ONLY requests submitted from: %s"%debug_ips)
    else:
        logger.info("Processing all, EXCEPT requests submitted from: %s"%debug_ips)
    while True:
        m = provaas_queue.read(max_processing_seconds) # we estimate that processing this message will take less than that seconds
        if m is None:
            sleep(waiting_seconds) # wait 5 minutes, maybe requests will come
            logger.info(".")
            bRequiresNewLine = True
            continue

        if bRequiresNewLine: print
        bRequiresNewLine = False
        obj_received = json.loads(m.get_body())
        requestIP = obj_received['requestIP']
        method = obj_received.get('method','POST')
        submitAt = obj_received['submitAt']
        user = obj_received['user']

        # debug_ips for test - ignored in provaas.org
        if requestIP in debug_ips:
            if not args.test:
                logger.info("Request from %s will be ignored for %s seconds"%(requestIP, max_processing_seconds) )
                continue
        else:
            if args.test:
                logger.info("Request from %s will be ignored for %s seconds"%(requestIP, max_processing_seconds) )
                continue

        # all other IPs are processed normally

        processing_starttime = datetime.now()
        # logger.info("Start processing at: %s"% processing_starttime.strftime("%Y-%m-%d %H:%M:%S.%f" ) )

        identifier = 'NONE'
        if method == 'POST':
            count_post += 1
            obj = obj_received['obj']
            requestId = obj_received['requestId']
            identifier = '%s id %s'%(method,requestId)
            logger.info( "Processing id={requestId}, method={method}, submitted from {requestIP} at {submitAt} by {user}, containing: {obj}".format(
                   requestId=requestId, method=method, requestIP=requestIP,submitAt=submitAt,user=user,obj=json.dumps(obj) ) )
            process_post(obj)
        elif method == "DELETE_BY_UUID":
            count_delete += 1
            namespace = obj_received['namespace']
            uuid = obj_received['uuid']
            identifier = '%s uuid %s ,namespace %s'%(method, uuid, namespace)
            logger.info( "Processing uuid={uuid}, namespace={namespace}, method={method}, submitted from {requestIP} at {submitAt} by {user}".format(
                   uuid=uuid, namespace=namespace, method=method, requestIP=requestIP,submitAt=submitAt,user=user ) )
            process_delete_by_uuid(namespace,uuid)
        elif method == "DELETE_BY_RID":
            count_delete += 1
            namespace = obj_received['namespace']
            rid = obj_received['rid']
            identifier = '%s rid %s ,namespace %s'%(method, rid, namespace)
            logger.info( "Processing rid={rid}, namespace={namespace}, method={method}, submitted from {requestIP} at {submitAt} by {user}".format(
                   rid=rid, namespace=namespace, method=method, requestIP=requestIP,submitAt=submitAt,user=user ) )
            process_delete_by_rid(namespace,rid)
        else:
            count_unknown += 1
            logger.info( "Don't know how to handle message={obj}".format(obj=json.dumps(obj_received)) )

        provaas_queue.delete_message(m)
        processing_endtime = datetime.now()
        logger.info("Processing %s took: %s"%( identifier,str(processing_endtime-processing_starttime)) )
        if (count_delete+count_post+count_unknown)%10 == 0 :
            print_statistics(count_post, count_delete, count_unknown)

if __name__ == '__main__':
    setup_golbal_variables()

    parser = argparse.ArgumentParser()
    parser.add_argument("--test", help="start server in test mode")
    args = parser.parse_args()
    if args.test:
        logger.info("Starting server in TEST mode")
    else:
        logger.info("Starting server in NORMAL mode")

    processing_loop(args.test)
